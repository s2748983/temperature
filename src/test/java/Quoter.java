public class Quoter {
    double getCelsiusToFahrenheit(double celsius) {
        double fahrenheit = celsius * 1.8 + 32;
        return fahrenheit;
    }
}
