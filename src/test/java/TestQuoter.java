import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {

    @Test
    public void testBook1 () throws Exception {
        Quoter quoter = new Quoter();
        double temp = quoter.getCelsiusToFahrenheit(1.0);
        Assertions.assertEquals(10.0, temp , 0.0,"Price_of_book_1");
    }
}
